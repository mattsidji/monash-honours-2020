import os.path
import time
from utils import *

#connect the PostgreSQL database 
conn = database_connect()

#Manual landmark name input  
""" manual_input(conn) """

# This waits for the seq2seq output file to exist then it reads it.

finished = False
takeoff =False
while not finished:
    print('waiting for instruction')
    while not os.path.exists('LTL/attention/output/seq2seq_output.txt'):
        time.sleep(1)
    if os.path.isfile('LTL/attention/output/seq2seq_output.txt'):
        with open('LTL/attention/output/seq2seq_output.txt', 'r') as file:
            example_input = file.read()#.replace('\n', '') 
        print(example_input)
    else:
        raise ValueError("%s isn't a file!" % 'LTL/attention/output/seq2seq_output.txt')


    # example_input = "f ( lm( veterans memorial hall )lm ) "

    # This runs functions from utils.py returns a Latlong for Make waypoints. 
    LTL2latlong(example_input, conn)

    # This turns the waypoint into the correct file format to input to Arducopter.

    MakeWaypoints(takeoff)

    # Finally this removes the file from LTL/output.
    if os.path.isfile ('LTL/attention/output/seq2seq_output.txt'):
        os.remove('LTL/attention/output/seq2seq_output.txt')
    # finished=False

