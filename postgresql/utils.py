import pandas as pd
import cred_psql as creds
import psycopg2
import numpy as np
# fuzzywuzzy adds fuzzy string checks
from fuzzywuzzy import fuzz
from fuzzywuzzy import process


# import pandas.io.sql as psql
def database_connect():
    ##Connect to the PostgreSQL database of OSM to get the latitude and longitude for each place. 
    conn_string = "host="+ creds.PGHOST + " port="+ "5432" +" dbname="+ creds.PGDATABASE + " user=" + creds.PGUSER \
        +" password="+ creds.PGPASSWORD

    conn=psycopg2.connect(conn_string)
    print("Connected!")
    #Create a cursor object
    # cursor = conn.cursor()
    return conn

def LTL_dict(LTL:str):
    sent = LTL.split(' ')
    extract = False
    order_no = 0
    D = {}
    for idx,token in enumerate(sent):
        if token.startswith('lm('):
            extract = True
            df = str()
            command = sent[idx - 2]
        elif token.startswith(')lm'):
            extract = False
            df = df.lstrip()
            D[order_no] = {'landmark': df, 'command': command}
            order_no =+ 1
        elif extract == True:
            df += ' '+ sent[idx]
    print(D)
    return D

def LatLongSearch(name, conn):
    
    # Adding in a check to see if name is a polygon or a point
    point = False #NOTE: Initilises if point or poly
    polygon = False
    fuzzy = False #NOTE: Inits if it's a fuzzy search

    #First performs a direct search
    poly_counter = pd.read_sql("SELECT COUNT(*) FROM planet_osm_polygon WHERE name = '{}';".format(str(name)), conn)
    point_counter = pd.read_sql("SELECT COUNT(*) FROM planet_osm_point WHERE name = '{}';".format(str(name)), conn)

    poly_counter = int(poly_counter.to_string(header=False, index=False)) 
    point_counter = int(point_counter.to_string(header=False, index=False)) 


    # Checks if there is EXACTLY 1 name associated with input landmark.
    if poly_counter != 0:
        polygon = True
        polynames = pd.read_sql("SELECT name FROM planet_osm_polygon WHERE name = '{}';".format(str(name)), conn).to_string(header=False, index=False)
        polynames = polynames.strip()
        print(polynames)
    elif point_counter != 0:
        point = True
        pointnames = pd.read_sql("SELECT name FROM planet_osm_point WHERE name = '{}';".format(str(name)), conn).to_string(header=False, index=False)
        pointnames = pointnames.strip()
        print(pointnames)
    else: # If direct search fails, conduct fuzzy search
        print('No exact match, conducting fuzzy search')
        fuzzy_poly_counter = pd.read_sql("SELECT COUNT(*) FROM planet_osm_polygon WHERE name % '{}';".format(str(name)), conn)
        fuzzy_point_counter = pd.read_sql("SELECT COUNT(*) FROM planet_osm_point WHERE name % '{}';".format(str(name)), conn)
        poly_counter = int(fuzzy_poly_counter.to_string(header=False, index=False)) 
        point_counter = int(fuzzy_point_counter.to_string(header=False, index=False)) 
        fuzzy = True
        if poly_counter != 0:
            polygon = True
            print("There are {} Polygons with similar names to {}".format(poly_counter, name))
            polynames = pd.read_sql("SELECT name FROM planet_osm_polygon WHERE name % '{}';".format(str(name)), conn).to_string(header=False, index=False)
            polynames = polynames.split('\n')
            polynames = [x.strip(' ') for x in polynames]
            print(polynames)

            #This checks if there is a name 'similar enough' to the input name (roughly 75% similar)
            ratio = 0
            ratio_list = []
            for i in polynames:
                if fuzz.ratio(str(name), i) > ratio:
                    ratio = fuzz.ratio(str(name), i)
                    ratio_list.append(ratio)
                    best_ratio = i
            if ratio > 75:
                polynames = [best_ratio]
                print("The similarity ratios are {}".format(ratio_list))
                print("There is a name similar to {}: {}, with a ratio of {}".format(name, best_ratio, ratio_list[1]))
            
        elif point_counter != 0:
            point = True
            print("There are {} Points with similar names to {}".format(point_counter, name))
            pointnames = pd.read_sql("SELECT name FROM planet_osm_point WHERE name % '{}';".format(str(name)), conn).to_string(header=False, index=False)
            pointnames = pointnames.split('\n')
            pointnames = [x.strip(' ') for x in pointnames]
            print(pointnames)

            #This checks if there is a name 'similar enough' to the input name (roughly 75% similar)
            ratio = 0
            for i in pointnames:
                if fuzz.ratio(str(name), i) > ratio:
                    ratio = fuzz.ratio(str(name), i)
                    best_ratio = i
            if ratio > 75:
                pointnames = [best_ratio]
                print("There is a name similar to {}: {}".format(name, best_ratio))

        else:
            fuzzy = False
            print("Could not find specified landmark")
    

    #Print outputs if poly or point, sets the OSM table it should be querying.
    if polygon:
        #print('Landmark is a polygon')
        table = 'polygon'
    elif point:
        #print('Landmark is a point')
        table = 'point'
    
    # This needs changing. Not always the case if fuzzy > 1 landmarks.
    if fuzzy or polygon:
        #Below opens location data from Ardupilot and get the starting lat long of the drone.
        fileHandle = open ( 'Jay/ArduPilot/Tools/autotest/locations.txt',"r" )
        lineList = fileHandle.readlines()
        home_location = lineList[len(lineList)-1]
        fileHandle.close()

        foo = home_location.split('=')
        faa = foo[1].split(',')
        home_lat = float(faa[0])
        home_lon = float(faa[1])
    
    # Below is Postgresql command for getting lon lat from way column in plat_osm_point. Automatically gets the correct decimal place.
    if point:
        
        
        if fuzzy:
            list_of_latlongs = []
            for i in pointnames:
                sql_command = "SELECT ST_X (ST_Transform (way,4326)), ST_Y (ST_Transform (way,4326)) FROM planet_osm_" + table + " WHERE name = '{}';".format(i)
        # Load the data
            try:
                data = pd.read_sql(sql_command, conn)#NOTE: add a error message to retry if not work... 
            except ValueError:
                print('No landmark could be found')
                exit()
        #Clean the data 
            data_clean = data.to_string(header=False, index=False).lstrip()

            try:
                latlong =  data_clean.split('  ')
                list_of_latlongs.append(latlong)
            except:
                print('Could not convert to Float, check to see if data is NULL')
                exit()

            for i in range(len(list_of_latlongs)):

                #list_of_latlongs[i] = list_of_latlongs[i].split(' ')
                x = home_lat - float(list_of_latlongs[i][1])
                y = home_lon - float(list_of_latlongs[i][0])
                distance = np.sqrt((x) ** 2 + (y) ** 2)
                print("Distance from home to {} is {}".format(pointnames[i],distance))
                if i == 0:
                    k = distance
                    index = i
                else:
                    if k > distance:
                        k = distance
                        index = i

                finallatlong = float(list_of_latlongs[index][0]), float(list_of_latlongs[index][1])    
                output = str(finallatlong[1])+' '+str(finallatlong[0])

        else:
            sql_command = "SELECT ST_Y (ST_Transform (way,4326)), ST_X (ST_Transform (way,4326)) FROM planet_osm_" + table + " WHERE name = '{}';".format(pointnames)
               
        # Load the data
            try:
                data = pd.read_sql(sql_command, conn)#NOTE: add a error message to retry if not work... 
            except ValueError:
                print('No landmark could be found')
                exit()
        #Clean the data 
            data_clean = data.to_string(header=False, index=False).lstrip()
            output = data_clean.replace('  ', ' ')
            try:
                latlong =  data_clean.split('  ')
                #output = latlong
            except:
                print('Could not convert to Float, check to see if data is NULL')
                exit()
        

    
    elif polygon:
        #Inits the list of closest lat longs of the polygons
        list_of_latlongs = []

        
        
        for i in polynames:
<<<<<<< HEAD
            
=======
            i = i.replace("'", "") 
>>>>>>> df6c53db9187a20cba06026ea0edddb3df74c2df
            if fuzzy:
                sql_command = "SELECT substring(left(ST_AsText(ST_Transform(way,4326)), -2), 10) FROM planet_osm_" + table + " WHERE name = '{}';".format(i)
            else:
                sql_command = "SELECT substring(left(ST_AsText(ST_Transform(way,4326)), -2), 10) FROM planet_osm_" + table + " WHERE name = '{}';".format(polynames)
        #Load the data
            try:
                data = pd.read_sql(sql_command, conn)#NOTE: add a error message to retry if not work... 
            except ValueError:
                print('No landmark could be found')
                exit()
            data_clean = data.to_string(header=False, index=False).lstrip()
            poly_points =  data_clean.split(',')

            #Below iterates through every poly_node and check which is closest to drone
            for i in range(len(poly_points)):
                poly_points[i] = poly_points[i].split(' ')
                x1 = poly_points[i][1]
                x_1 = float(x1)
                y1 = poly_points[i][0]
                y_1 = float(y1)

                x = home_lat - x_1
                y = home_lon - y_1
                distance = np.sqrt((x) ** 2 + (y) ** 2)
                if i == 0:
                    k = distance
                    index = i
                else:
                    if k > distance:
                        k = distance
                        index = i
           
            latlong = float(poly_points[index][0]), float(poly_points[index][1])    
            output = str(latlong[1])+' '+str(latlong[0]) #Fliped indecies because planet_osm_points does long lat instead of lat long
            list_of_latlongs.append(output)
        if fuzzy:
            for i in range(len(list_of_latlongs)):

                list_of_latlongs[i] = list_of_latlongs[i].split(' ')
                x = home_lat - float(list_of_latlongs[i][1])
                y = home_lon - float(list_of_latlongs[i][0])
                distance = np.sqrt((x) ** 2 + (y) ** 2)
                print("Distance from home to {} is {}".format(polynames[i],distance))
                if i == 0:
                    k = distance
                    index = i
                else:
                    if k > distance:
                        k = distance
                        index = i

                finallatlong = float(list_of_latlongs[index][0]), float(list_of_latlongs[index][1])    
                output = str(finallatlong[0])+' '+str(finallatlong[1]) #Fliped indecies because planet_osm_points does long lat instead of lat long
        else:
            list_of_latlongs = list_of_latlongs[0]
        
        """
        average_long = 0.0 #NOTE: Inits average lat, longs.
        average_lat = 0.0

        #This just loops through each lat and long and averages the value out to get 1 lat long.
        for i in range(len(poly_points)):
            poly_points[i] = poly_points[i].split(' ')
                        
            average_long += float(poly_points[i][0])
            average_lat += float(poly_points[i][1])
        
        average_lat = average_lat / len(poly_points)
        average_long = average_long / len(poly_points)

        latlong = (average_long,average_lat)
        """
    
        print("Here's a list of closest coordinates for each polygon {}".format(list_of_latlongs))
    
    #This now sorts the list of closest points in in each Polygon, which out of these is the closest polygon.
    if fuzzy and polygon:
        print("The closest polygon is {}, located at {}".format(polynames[i], output))
    elif polygon:
        print("The closest polygon is {}, located at {}".format(polynames, output))
    elif (fuzzy or point or polygon) == False:
        exit()
    else:
        print("The closest point is {}, located at {}".format(pointnames, output))
    
    return (output)

def FenceFunction(name, conn):
    #connects to data base.

    poly_counter = pd.read_sql("SELECT COUNT(*) FROM planet_osm_polygon WHERE name = '{}';".format(str(name)), conn)
    poly_counter = int(poly_counter.to_string(header=False, index=False))
     #runs a direct query to find OSM polygon that matches name.
    if poly_counter != 0:
        polynames = pd.read_sql("SELECT name FROM planet_osm_polygon WHERE name = '{}';".format(str(name)), conn).to_string(header=False, index=False)
        polynames = polynames.strip()
        print(polynames)
    #if fails runs a fuzzy query.
    else: # If direct search fails, conduct fuzzy search
        print('No exact match, conducting fuzzy search')
        fuzzy_poly_counter = pd.read_sql("SELECT COUNT(*) FROM planet_osm_polygon WHERE name % '{}';".format(str(name)), conn)
        poly_counter = int(fuzzy_poly_counter.to_string(header=False, index=False)) 
        fuzzy = True
        if poly_counter != 0:
            print("There are {} Polygons with similar names to {}".format(poly_counter, name))
            polynames = pd.read_sql("SELECT name FROM planet_osm_polygon WHERE name % '{}';".format(str(name)), conn).to_string(header=False, index=False)
            polynames = polynames.split('\n')
            polynames = [x.strip(' ') for x in polynames]
            print(polynames)

            ratio = 0
            for i in polynames:
                if fuzz.ratio(str(name), i) > ratio:
                    ratio = fuzz.ratio(str(name), i)
                    best_ratio = i
            if ratio > 75:
                polynames = [best_ratio]
                print("There is a name similar to {}: {}".format(name, best_ratio))
    
    #extract lat longs of polygon into list
    list_of_latlongs = []
    sql_command = "SELECT substring(left(ST_AsText(ST_Transform(way,4326)), -2), 10) FROM planet_osm_polygon WHERE name = '{}';".format(polynames[0])
    
    try:
        data = pd.read_sql(sql_command, conn)#NOTE: add a error message to retry if not work... 
    except ValueError:
        print('No landmark could be found')
        exit()
    #output that list
    data_clean = data.to_string(header=False, index=False).lstrip()
    poly_points =  data_clean.split(',')
    return(poly_points)


def LTL2latlong(LTL:str, conn):
    extract_LTL = LTL_dict(LTL)
    f_out = open("postgresql/latlong.txt", "w")
    fence_out = open("Jay/ArduPilot/ArduCopter/waypoints/fence/avoid.txt", "w")

    for i in extract_LTL:
        place = extract_LTL[i]['landmark']
        place  = " ".join(w.capitalize() for w in place.split())#capitalise each word in the string
        instruction = extract_LTL[i]['command']
        
        # If it's an avoid do the avoid extract.
        if instruction == '~':
            fence = FenceFunction(place, conn)
            #^ will output a list
            #write each element of the list as a single line in fence.txt
            for coord in fence:
                fence_out.write("%s\n" % coord)
        # If it's a go to do the go to extract.
        elif instruction == 'f':
            output = LatLongSearch(place, conn)
            if i == 0:
                f_out.write(output + '\n')#used for arbitrary starting point
                f_out.write('0.000000 0.000000\n')#used for takeoff command
            f_out.write(output + '\n')
 
    f_out.close()
    fence_out.close()


def MakeWaypoints(takeoff):
    #set start and end files
    df = pd.read_csv('postgresql/latlong.txt', sep=" ", header=None)
    f_out = open("Jay/ArduPilot/ArduCopter/waypoints/OSM/output.txt", "w")
    #write Waypoint File Header
    f_out.writelines('QGC WPL 110\n')

    #index control
    param9_x_lat = df.iloc[:,0]
    param10_y_long = df.iloc[:,1]

    param1_index = 0
    param2_currentWP = 0
    param3_coord_frame = np.array([0,3])#used for c
    param4_command = np.array([16,82,177,22])#used for b
    param5 = np.array([0,1])#used for a
    param6 = np.array([0,-1])#used for a
    param7 = 0
    param8 = 0
    param11_x_altitude = np.array([10, 10, 0, 10])#use for b
    param12_autocontinue = 1
    for i in range(len(df)):
        a=0
        if i == 0 : #arbitrary starting point
            a=0
            b=0
            c=0
        elif i == 1 and takeoff ==False: #takeoff command
            a=0
            b=3
            c=1
            takeoff = True
        # elif i == len(df):
        #     a=0
        #     b=2
        #     print(i)
        else: #waypoint go to comand 
            a=1
            b=1
            c=1
        
        f_out.writelines(str(param1_index)+ "	"+str(param2_currentWP)+ "	" 
        +str(param3_coord_frame[c])+ "	" 
        +str(param4_command[b])+ "	" 
        +str(param5[a])+ "	"+str(param6[a])+ "	" 
        +str(param7)+ "	"+str(param8)+ "	" 
        +str(param9_x_lat[i])+ "	"+str(param10_y_long[i])+ "	" 
        +str(param11_x_altitude[b])+ "	" 
        +str(param12_autocontinue)+'\n')
        # f_out.writelines(str(df.iloc[i,0])+ "	" + str(df.iloc[i,1])+'\n')
        param1_index +=1 


    f_out.close()

def inputNumber(message):
  while True:
    try:
       userInput = int(input(message))       
    except ValueError:
       print("Not an integer! Try again.")
       continue
    else:
       return userInput 
       break 

def manual_input(conn):
    f_out = open("latlong.txt", "w")
    x = inputNumber('How many landmarks do you want to fly to? ')
    for i in range(x):
        place = input('What place do you want to find? ')
        output = LatLongSearch(place, conn)
        if i ==0:
            f_out.write(output)
        f_out.write(output)
    f_out.close()




