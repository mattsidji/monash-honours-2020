#extract the data into the desired format 
import os 
os.chdir('seq2tree/LTL/data/Prepare/')
import numpy
import pandas as pd
from sklearn.model_selection import train_test_split


#Load data to dataframe
df1 = pd.read_csv('south_src.txt', sep="\n", header=None)
df2 = pd.read_csv('south_tar.txt', sep="\n", header=None)

df3 = pd.merge(df1, df2, left_index=True, right_index=True)
# print(df3.head())
# print(df3.shape)

# print('this is where I cut it')
# df3 = df3.head(100)

#some of the dataset has a " ." at the end of the string, we want to remove this:
df3['0_x'] = df3['0_x'].str.rstrip(' .')
df3['0_x'] = df3['0_x'].str.lower()
df3['0_y'] = df3['0_y'].str.lower()

#remove rows with undesirable landmarks
# df3 = df3[~df3['0_x'].str.contains("_floor")]

# Split our source and target files with a 20:80 split
train, test = train_test_split(df3, test_size=0.2)

##Substitute some of the testing data with unseen landmarks
#randomly select 50% of testing data 
# print(test.shape)
test = test.sample(frac = 0.5) 


#Load dataset with unseen dataset:
df4 = pd.read_csv('north_some_south_src.txt', sep="\n", header=None)
df5 = pd.read_csv('north_some_south_tar.txt', sep="\n", header=None)

df6 = pd.merge(df1, df2, left_index=True, right_index=True)

# print('this is where I cut the north landmarks')
# df6 = df6.head(100)

#remove rows with undesirable landmarks
# df6 = df6[~df6['0_x'].str.contains("_floor")]
df6 = df6[~df6['0_x'].str.contains("Berk's;Shanghai")]
# print(test.shape)

#some of the dataset has a " ." at the end of the string, we want to remove this:
df6['0_x'] = df6['0_x'].str.rstrip(' .')

#append the same amount of data removed with randomly sampled unseen data 
test = test.append(df6.sample(n = int(test.shape[0])), ignore_index=True)
test['0_x'] = test['0_x'].str.lower()
test['0_y'] = test['0_y'].str.lower()

# print(test.shape)

#sort by source acending string length
s = train['0_x'].str.len().sort_values().index
train_order = train.reindex(s)
s = test['0_x'].str.len().sort_values().index
test_order = test.reindex(s)
# print(test_order)

#split source and target into individual text files
train_src = train_order['0_x'].tolist()
train_tgt = train_order['0_y'].tolist()

test_src = test_order['0_x'].tolist()
test_tgt = test_order['0_y'].tolist()


### Combine to make source \t target text file
f_out = open("train.txt", "w")

#added with zip
for a, b in zip(train_src, train_tgt):
    f_out.writelines(a+ "	"+b+'\n')
 
    #print(a,"  ", b,'\n')

f_out.close()


y_out = open("test.txt", "w")

#added with zip
for a, b in zip(test_src, test_tgt):
    y_out.writelines(a+ "	"+b+'\n')
 
    #print(a,"  ", b,'\n')

y_out.close()


### Count frequency of the words and symbols - Target
# Create an empty dictionary 
d = dict() 
  
# Loop through each line of the file 
for line in train_tgt: 
    # Remove the leading spaces and newline character 
    line = line.strip() 
  
    # Convert the characters in line to  
    # lowercase to avoid case mismatch 
    # line = line.lower() 
  
    # Split the line into words 
    words = line.split(" ") 
  
    # Iterate over each word in line 
    for word in words: 
        #Only select words with a length
        if len(word) >=1:
            # Check if the word is already in dictionary 
            if word in d: 
                # Increment count of word by 1 
                d[word] = d[word] + 1
            else: 
                # Add the word to dictionary with count 1 
                d[word] = 1
        else:
            continue

#sort the dictionary so that the frequency is in decending order
d_sorted_keys = sorted(d, key=d.get, reverse=True)


# Print the contents of dictionary to txt file 
with open("vocab.f.txt","w") as f:
    for key in d_sorted_keys:
        f.write("{}	{}\n".format(key,d[key]))


### Count frequency of the words and symbols - Source
# Create an empty dictionary 
d = dict() 
  
# Loop through each line of the file 
for line in test_src: 
    # Remove the leading spaces and newline character 
    line = line.strip() 

    # Convert the characters in line to  
    # lowercase to avoid case mismatch 
    # line = line.lower() 
  
    # Split the line into words 
    words = line.split(" ") 

    # Iterate over each word in line 
    for word in words: 
        #Only select words with a length
        if len(word) >=1:
            # Check if the word is already in dictionary 
            if word in d: 
                # Increment count of word by 1 
                d[word] = d[word] + 1
            else: 
                # Add the word to dictionary with count 1 
                d[word] = 1
        else:
            continue

#sort the dictionary so that the frequency is in decending order
d_sorted_keys = sorted(d, key=d.get, reverse=True)

# Print the contents of dictionary to txt file 
with open("vocab.q.txt","w") as f:
    for key in d_sorted_keys:
        f.write("{}	{}\n".format(key,d[key]))

##OLD way with txt files 
# input = open("twophrase_south_clean_src.txt", "r")
# output = open("twophrase_south_clean_tar.txt", "r")

# #sort in terms of length of input while keeping the index the same with the output 
# temp = sorted(zip(input, output), key=lambda x: len(x[0]))
# in_list, out_list = map(list, zip(*temp))