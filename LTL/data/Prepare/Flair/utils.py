import pandas as pd
# import os 
# os.chdir('seq2tree/LTL/data/Prepare/Flair/')
import pprint
import json

#Build the dictionary to be able to call and replace the symbol 
class NERMap:
    def __init__(self):
        self._forwards = []
        self._backwards = {}
    def _call_(self, word: str):
        if word not in self._backwards:
            label = len(self._forwards)
            self._backwards[word] = label
            self._forwards.append(word)
        return f"${self._backwards[word]}"
    def __getitem__(self, label: str):
       idx = int(label.replace('$',''))
       return self._forwards[idx]

#Search for the entities and replace with an ID for each sentence
class entity_swap:
    def __init__(text):
        ner = NERMap()
        id_sent = []
        key = []
        key_label = []
        df=str()
        for i in text:
            flag = False
            new_sent = []
            counter = {}
            counter_label = {}
            sent = i.split(' ')
            symb = 0
            for idx,token in enumerate(sent):
                if token.startswith('<B-'):  
                    if sent[idx-1] != 'the':
                        new_sent.pop()
                        df += " " + sent[idx-1]
                elif token.startswith('<I-'):
                    new_sent.pop()
                    df += " " + sent[idx-1]

                elif token.startswith('<E-'):
                    new_sent.pop()
                    df += " " + sent[idx-1]
                    label = token[3:len(token)-1]
                    counter_label[ner._call_(df)] = [df, label]
                    # counter[ner._call_(df)] = df
                    a = label + str(symb)
                    counter[a] = df
                    symb += 1
                    # new_sent.append(ner._call_(df))
                    new_sent.append(a)
                    df=str()
                elif token.startswith('<S-'):
                    new_sent.pop()
                    df += " " + sent[idx-1]
                    label = token[3:len(token)-1]
                    counter_label[ner._call_(df)] = [df, label]
                    # counter[ner._call_(df)] = df
                    # new_sent.append(ner._call_(df))
                    a = label + str(symb)
                    counter[a] = df
                    new_sent.append(a)
                    symb += 1
                    df=str()
                else:
                    new_sent.append(token)
                if token == '\n':
                    id_sent.append(' '.join(new_sent))
                    key.append(counter)
                    key_label.append(counter_label)
                    # print(new_sent)
        return id_sent, key, key_label

    def __save__(id_sent, save_name):
        #save the text with entities as key 
        with open('%s.txt' % (save_name), 'w') as f:
            for items in id_sent:
                f.write(items.lstrip())
        print('successfully saved %s' % save_name)
        return print()
    def __id_to_entity__(data_id, data_dict):
        id_removed = []
        for i in range(len(data_id)):
            temp = data_id[i]
            count=0
            for symbol, entity in data_dict[i].items():
                temp = temp.replace(symbol, entity)
                count +=1
                if count==len(data_dict[i]):
                    id_removed.append(temp)
        return id_removed
    def __raw_dict_to_id__(data_raw, data_dict):
        id_added = []
        for i in range(len(data_raw)):
            temp = data_raw[i]
            # count=0
            for symbol, entity in data_dict[i].items():
                temp = temp.replace(entity.strip(), symbol)
                # if entity in temp:
                    # count += 1
                # count +=1
                # if count==len(data_dict[i]):
            id_added.append(temp)

        return id_added
    def extract_element(data1, feature):
        data_feature = []
        for element in data1:
            data_feature.append(data1[str(element)][str(feature)])
        return data_feature