import pandas as pd
from utils import *



import nltk
import sklearn_crfsuite

from copy import deepcopy
from collections import defaultdict

from sklearn_crfsuite.metrics import flat_classification_report

from ner_evaluation.ner_eval import collect_named_entities
from ner_evaluation.ner_eval import compute_metrics
from ner_evaluation.ner_eval import compute_precision_recall_wrapper


print(list(nltk.corpus.conll2002.iob_sents('esp.testb')))
exit()

df = pd.read_csv('seq2tree/LTL/data/Prepare/Flair/src_flair_ner-ontonotes_full.txt', sep="\n", header=None)
df[0] = df[0].str.rstrip(' .')
text = df.head(100).to_string(index=False, header=False).splitlines()
text = [item + " \n" for item in text]
text = list(map(str.lstrip, text))
id_sent, key, key_label = entity_swap.__init__(text)



df1 = pd.read_csv('seq2tree/LTL/data/Prepare/Flair/NER_accuracy_gold.txt', sep="\n", header=None)
df1[0] = df[0].str.rstrip(' .')
text1 = df.head(100).to_string(index=False, header=False).splitlines()
text1 = [item + " \n" for item in text]
text1 = list(map(str.lstrip, text))
id_sent1, key1, key_label1 = entity_swap.__init__(text)

key1 = key1[0:10]
key = key[1:10]
print(key1[0:10])
print(key_label1[0])
correct=0
num=0
for idx, i in enumerate(key):
    gold = key1[idx]
    print('gold', gold)
    print('i:', i)
    reverse_i = {v: k for k, v in i.items()}
    print('reverse i ', reverse_i)
    print('len i', len(i.keys()))
    num+=len(i.keys())
    for entity, symbol in gold.items():
        if symbol in reverse_i: 
            correct+=1
            print('symbol', symbol)

print(correct/num)
# print(key)
# print('.................................')
# print(key1)

# '''extract key label'''
# for data_d in key_label:
#         print(len(data_d))
#     for i, y in data_d.items():
#         print(y[1])
