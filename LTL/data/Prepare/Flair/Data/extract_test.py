##from flair, we extract the entities into symbols, which is then converted back from .json file

import pandas as pd
import os 
os.chdir('seq2tree/LTL/data/Prepare/Flair/')


#Build the dictionary to be able to call and replace the symbol 
class NERMap:
    def __init__(self):
        self._forwards = []
        self._backwards = {}
    def _call_(self, word: str):
        if word not in self._backwards:
            label = len(self._forwards)
            self._backwards[word] = label
            self._forwards.append(word)
        return f"${self._backwards[word]}"
    def __getitem__(self, label: str):
       idx = int(label.replace('$',''))
       return self._forwards[idx]
# flair_t = open("listfile_flair_ner-ontonotes.txt") 
example_sent1 = "Head straight to the <B-FAC> Bakery <E-FAC> past the <B-FAC> Hype <I-FAC> Collection <E-FAC>  . \n"
# example_sent2 = "Head straight to the <S-FAC> Bakery  past the Unique Hype Collection ."
# example_sent3 = CASE WITH <S- 

df = pd.read_csv('listfile_flair_ner-ontonotes.txt', sep="\n", header=None)


print('NOTE this is where the length is sortened with head')
text = df.to_string(index=False, header=False).splitlines()
#add \n to the end of the string
text = [item + " \n" for item in text]

#Search for the entities and replace with an ID for each sentence
ner = NERMap()
id_sent = []
key = []
df=str()

for i in text:
    flag = False
    new_sent = []
    counter = {}
    entity_idx = 0
    sent = i.split(' ')
    for idx,token in enumerate(sent):
        if token.startswith('<B-'):  
            new_sent.pop()
            df += " " + sent[idx-1]
            # print(new_sent.pop())
            # flag=True
            # continue
        elif token.startswith('<I-'):
            new_sent.pop()
            df += " " + sent[idx-1]
            # flag=True     
            # continue
        elif token.startswith('<E-'):
            new_sent.pop()
            df += " " + sent[idx-1]
            # flag=False
            # print(df)
            # print(ner._call_(df))
            counter[ner._call_(df)] = df
            # print(counter)
            new_sent.append(ner._call_(df))
            df=str()
        elif token.startswith('<S-'):
            new_sent.pop()
            df += " " + sent[idx-1]
            counter[ner._call_(df)] = df
            new_sent.append(ner._call_(df))
            df=str()
        else:
            new_sent.append(token)

            
        """
        if token.startswith('<S-'):
            
            # print(df)
            # print(ner._call_(df))
            counter[ner._call_(new_sent.pop())] = new_sent.pop()
            print(counter)
            new_sent.append(ner._call_(new_sent.pop()))
            df=str()

            continue
        """
        # if flag:
            # df += " " + token 
            # assert idx - 1 >= 0
            # df += " " + sent[idx-1]
        # else:
            # new_sent.append(token)

        if token == '\n':
            id_sent.append(' '.join(new_sent))
            key.append(counter)
    


#save the text with entities as key 
with open('anon_text.txt', 'w') as f:
    for items in id_sent:
        f.write(items.lstrip())

#Key is the key for the dictionay and is now split into
print(key)

#remove empty space on left of output
id_sent = list(map(str.lstrip, id_sent))
text = list(map(str.lstrip, text))
print(id_sent)   

#convert this indo a nested dictionary json file so that we can easily access the corresponding dictionary values for each instance
nested = list(zip(text, id_sent, key))
cols = ['NER_raw', 'NER_id', 'dict_id']
data = pd.DataFrame(nested, columns = cols)
print(data)

data.to_json(r'nested.json', orient='index')

#re-extract for use 
import pprint
import json
data1 = json.load(open('nested.json', 'r+'))
print(pprint.pprint(data1))

data_id = []
data_dict = []
for element in data1:
    data_id.append(data1[str(element)]['NER_id'])
    data_dict.append(data1[str(element)]['dict_id'])
print(data_id)
print(data_dict)
exit()
##Replace Back 
print(data_id[1], data_dict[1].keys)

id_removed = []

for i in range(len(data_id)):
    temp = data_id[i]
    count=0
    for symbol, entity in data_dict[i].items():
        temp = temp.replace(symbol, entity)
        count +=1
        if count==len(data_dict[i]):
            id_removed.append(temp)

print(id_removed)

exit()

##Assume we have extracted it for id_sent and key.... 
print(id_sent[1], key[1].keys)

id_removed = []

for i in range(len(id_sent)):
    # find_replace_multi_ordered(id)
    # id_removed.append(id_sent[i], key[i])
    temp = id_sent[i]
    count=0
    for symbol, entity in key[i].items():
        temp = temp.replace(symbol, entity)
        count +=1
        if count==len(key[i]):
            id_removed.append(temp)

print(id_removed)

#save coundters 
#Print the identified entities 
# print(counter)
# for i in counter:
#     print(ner.__getitem__(i))    

# print(ner['$0'])

# ner = NERMap()
# flag = False
# df=str()
# counter=[]
# new_sent = []
# id_sent = []
# entity_idx = 0
# for token in example_sent1.split(' '):
#     if token.startswith('<B-'):  

#         flag=True
#         continue
#     if token.startswith('<I-'):
#         flag=True     
#         continue
#     if token.startswith('<E-'):
#         flag=False
#         # print(df)
#         # print(ner._call_(df))
#         counter.append(ner._call_(df))
#         new_sent.append(ner._call_(df))
#         df=str()

#         continue
#     if flag:
#         df += " " + token 
#     else:
#         new_sent.append(token)
#     if token == '\n':
#         id_sent.append(' '.join(new_sent))
# with open('anon_text.txt', 'w') as f:
#     f.write(id_sent[0])
# print(new_sent, id_sent)


# import re
# import functools

# text1 =  "blahblahblah $%word$% blablablabla $%car$%"
# #Case 1: only 1 word
# #Case 2: Head straight to the <B-FAC> Taipan <I-FAC> Bakery <E-FAC>
# ## = Head straight to the <B-FAC> Taipan <I-FAC> Bakery <E-FAC>
# ##-- Head straight to the $0

# ##extraction step 
# ## Replace step


# text = "Head straight to the <B-FAC> Bakery <E-FAC> past the Unique Hype Collection ."

# words = dict(word="wassup", car="toyota")

# class Replacement(object):
#     def __init__(self, table):
#         self.table = table
#     def __call__(self, match):
#         try:
#             return self.table[match.group(1)]
#         except:
#             return match.group(0)

# table = dict(word="wassup", car="toyota")
# pattern = re.compile('<B-[A-Z]+>\s(.*?)<E-[A-Z]+>')
# print(pattern.sub(ner._call_('*****'), text))
# result = pattern.sub(Replacement(table), text)
# print(result)