#https://github.com/flairNLP/flair/blob/master/resources/docs/TUTORIAL_4_ELMO_BERT_FLAIR_EMBEDDING.md
#https://github.com/flairNLP/flair/blob/1524941783214c1a4a2aa03d064e00e50821e20d/resources/docs/EXPERIMENTS.md
#Sencond link to add word embedding or BERT 

from flair.data import Sentence
from flair.models import SequenceTagger
import pandas as pd
import pickle
import os 
os.chdir('seq2tree/LTL/data/Prepare/Flair/')
from tqdm import tqdm ## tracks progress of loop ##

#additional NLTK tokenise
import nltk
from nltk import word_tokenize





#Load the dataset
df = pd.read_csv('south_src.txt', sep="\n", header=None)
# df = pd.read_csv('north_src.txt', sep="\n", header=None)

# pd.options.display.max_colwidth = 150 #to view data in full
# text = "get to Blue Room , while avoiding The Ivy Room"

df = df.head(50)
print('NOTE DF HAS BEEN CUT FOR NOW')

#Covert the dataframe into a list of strings
text = df.to_string(index=False, header=False).split('\n')

#Test if worked: 
print('Dataframe:', len(df))
print('Text:', len(text))
# for i in range(10):
#     print(text[i])
#     print('-'*10)


# load the NER tagger
tagger = SequenceTagger.load('ner-ontonotes')
# tagger = SequenceTagger.load('ner-ontonotes')
print(tagger)


#ner is 4 class NER with higher accuracy and ner-ontonotes is a 18 class NER 
# run NER over sentence to identify entities
##method 2:
#f for storing NER tagged string
f_NER = []
span_NER = []
#run for every sentence 
# for i in tqdm(range(len(text))):
#     sentence = Sentence(text[i])
#     tagger.predict(sentence)
#     #append tagged sentence
#     f_NER.append(sentence.to_tagged_string())
# df1 = pd.read_csv('seq2tree/LTL/data/Prepare/Flair/NER_accuracy_gold.txt', sep="\n", header=None)
# df1[0] = df[0].str.rstrip(' .')
# text1 = df.head(100).to_string(index=False, header=False).splitlines()
lines = []
tags_pred = []

for i in tqdm(range(len(text))):
    sentence = Sentence(text[i])
    # print(sentence) #list of tokens 
    tagger.predict(sentence)
    # make list of predicted tags
    predicted_spans = sentence.get_spans('ner')
    sentence_pred = []
    for token in sentence:
        predicted_tag = 'O'

        # check if in predicted spans
        for span in predicted_spans:
            if token in span:
                predicted_tag = 'B-' + span.tag if token == span[0] else 'I-' + span.tag
        sentence_pred.append(predicted_tag)
        lines.append(f'{token.text} {predicted_tag}\n')
    lines.append('\n') #BIO
    tags_pred.append(sentence_pred) #all predictions
    # for entity in sentence.get_spans('ner'):
    #         span_NER.append(entity)
# print(tags_pred)
# print(lines)#file in bio format


# with open('tags_pred.txt', 'w') as filehandle:
#     for i in tags_pred:
#         filehandle.write(i)
import  csv

with open("tags_pred.csv","w") as f:
    wr = csv.writer(f)
    wr.writerows(tags_pred)


with open("tags_actual_9.csv","r") as f:
    tags_true = list(csv.reader(f))



from seqeval.metrics import accuracy_score
from seqeval.metrics import classification_report
from seqeval.metrics import f1_score
print(tags_true)

print(f1_score(tags_true, tags_pred))
print(classification_report(tags_true, tags_pred))
print(accuracy_score(tags_true, tags_pred))
#Extension, I think I can add the evaluate here 
#FIRST, load golden standard
# from flair.datasets import DataLoader, CONLL_03



# df1 = pd.read_csv('NER_accuracy_gold.txt', sep="\n", header=None)
# #Covert the dataframe into a list of strings
# gold = df1.to_string(index=False, header=False).split('\n')
# #strip the blank space 
# gold = list(map(str.lstrip, gold))

# sentences = list(text)
# database

# result, score = tagger.evaluate(sentences=, out_path=f'gold_predictions.txt')

# print(tagger.evaluate(f_NER, gold ))
# # exit()

##check
'''
print('No: tagged sentences:', len(df))
print('No. input sentences:', len(text))
for i in range(5):
    print(f_NER[i])
    print(text[i])

with open('listfile_flair_ner-ontonotes_gold_test.txt', 'w') as filehandle:
    for i in f_NER:
        filehandle.write("%s\n" % i)




# tagger.predict(sentence)

# sentence
# print('The following NER tags are found:')

# # iterate over entities and print
# for entity in sentence.get_spans('ner-ontonotes'):
#     print(entity)


# output = sentence.to_tagged_string()
# print(output)
# exit()
# with open('listfile_flair_ner-ontonotes.txt', 'w') as filehandle:
#     filehandle.writelines(output)
#     #for listitem in sentence.to_tagged_string():
#      #   filehandle.write('%s\n' % listitem)
'''
# '''
# output = sentence.to_tagged_string()
# pickle.dump(output, open("flair_output.txt")'''