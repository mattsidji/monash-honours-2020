
from flair.data import Sentence
from flair.models import SequenceTagger
import pandas as pd
import pickle
import os 
os.chdir('seq2tree/LTL/data/Prepare/Flair/')
from tqdm import tqdm ## tracks progress of loop ##


# #Load the dataset input
src = pd.read_csv('south_src.txt', sep="\n", header=None)


#Covert the dataframe into a list of strings
text = src.to_string(index=False, header=False).split('\n')

# load the NER tagger
#ner is 4 class NER with higher accuracy and ner-ontonotes is a 18 class NER 
tagger = SequenceTagger.load('ner-ontonotes')
# tagger = SequenceTagger.load('ner-ontonotes')
print(tagger)


#f for storing NER tagged string
f_NER = []

# run for every sentence 
for i in tqdm(range(len(text))):
    sentence = Sentence(text[i])
    tagger.predict(sentence)
    #append tagged sentence
    f_NER.append(sentence.to_tagged_string())

with open('listfile_flair_ner-ontonotes_new.txt', 'w') as filehandle:
    for i in f_NER:
        filehandle.write("%s\n" % i)