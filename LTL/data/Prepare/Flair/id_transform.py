##from flair, we extract the entities into symbols, which is then converted back from .json file
import os 
import pandas as pd
# os.chdir('seq2tree/LTL/data/Prepare/Flair/')
# print(os.getcwd())


from utils import *

#Load data to dataframe

df1 = pd.read_csv('seq2tree/LTL/data/Prepare/Flair/south_src.txt', sep="\n", header=None)
df2 = pd.read_csv('seq2tree/LTL/data/Prepare/Flair/south_tar.txt', sep="\n", header=None)
df = pd.read_csv('seq2tree/LTL/data/Prepare/Flair/src_flair_ner-ontonotes_full.txt', sep="\n", header=None)

# #merge raw source and target
# df3 = pd.merge(df1, df2, left_index=True, right_index=True)

#remove rows with undesirable landmarks
# df3 = df3[~df3['0_x'].str.contains("_floor")]

#some of the dataset has a " ." at the end of the string, we want to remove this:
df1[0] = df1[0].str.rstrip(' .')
df[0] = df[0].str.rstrip(' .')


#covert dataframe into text:
print('NOTE this is where the length shortened')
text = df.to_string(index=False, header=False).splitlines()
raw_src = df1.to_string(index=False, header=False).splitlines()
raw_tgt = df2.to_string(index=False, header=False).splitlines()


##Pre-Processing
#add \n to the end of the string
text = [item + " \n" for item in text]

# remove the spaces that are before the start of the string
text = list(map(str.lstrip, text))
raw_src = list(map(str.lstrip, raw_src))
raw_tgt = list(map(str.lstrip, raw_tgt))
#use the entity swap function
id_sent, key, key_label = entity_swap.__init__(text)

# print(key)
# print(id_sent)

# print(key)
print(len(id_sent))
# entity_swap.__save__(id_sent, 'train_id_src')

#want target to have corresponding named entities
#dict + raw --> id form 
tgt_id = entity_swap.__raw_dict_to_id__(raw_tgt, key)

# print(tgt_id)
# entity_swap.__save__(tgt_id, 'train_id_tar')

#convert this indo a nested dictionary json file so that we can easily access the corresponding dictionary values for each instance
print('this should all be the same:', len(raw_src), len(text), len(id_sent), len(key), len(raw_tgt), len(tgt_id))
print('this should all be the same:', type(raw_src), type(text), type(id_sent), type(key), type(raw_tgt), type(tgt_id))
nested = list(zip(raw_src, text, id_sent, key, raw_tgt, tgt_id))
cols = ['src_orig', 'NER_flair', 'src_id', 'dict_id', 'tar_orig', 'tar_id']
data = pd.DataFrame(nested, columns = cols)
# print(data)
print(data.shape)


data_json = data.to_json(r'seq2tree/LTL/data/Prepare/Flair/src_nested_space.json', orient='index')
count= 0
# for key, data_dict in data.items():
#     if data_dict['tar_id'] == data_dict['tar_orig']:
#         data_dict['src_id'] = data_dict['src_orig']
#         count += 1

for i in range(len(data)):
    if data['tar_id'][i] == data['tar_orig'][i]:
        # print('this is the pair')
        # print(data['src_id'][i])
        # print(data['tar_id'][i])
        data['src_id'][i] = data['src_orig'][i]
        
        count += 1
print(count)
print(data.shape)
data = data[~data['tar_orig'].str.contains("first_floor")]
data = data[~data['tar_orig'].str.contains("second_floor")]
data = data[~data['src_orig'].str.contains("changing_floors")]

print(data.shape)
data = data[~data['src_orig'].str.contains("P-131317")]
print(data.shape)

#reset the index to make it easier for extraction (drop=True) means old index is not saved as column
data = data.reset_index(drop=True)

count =0
for i in range(len(data)):
    if data['tar_id'][i] == data['tar_orig'][i]:
        # print('this is the pair')
        # print(data['src_id'][i])
        # print(data['tar_id'][i])
        data['src_id'][i] = data['src_orig'][i]
        
        count += 1
print(count)
print(data[['src_id', 'tar_id']].head(10))
data.to_json(r'seq2tree/LTL/data/Prepare/Flair/src_nested_space.json', orient='index')




# #Check: Open json and extract elements 
# import pprint
# import json
# data1 = json.load(open('seq2tree/LTL/data/Prepare/Flair/src_nested.json', 'r+'))
# print(pprint.pprint(data1))

# tar = entity_swap.extract_element(data1, 'tar_id')


# print(tar)
