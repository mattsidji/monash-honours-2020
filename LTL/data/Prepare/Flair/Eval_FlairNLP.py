#https://github.com/flairNLP/flair/blob/master/resources/docs/TUTORIAL_4_ELMO_BERT_FLAIR_EMBEDDING.md
#https://github.com/flairNLP/flair/blob/1524941783214c1a4a2aa03d064e00e50821e20d/resources/docs/EXPERIMENTS.md
#Sencond link to add word embedding or BERT 

from flair.data import Sentence
from flair.models import SequenceTagger
import pandas as pd
import pickle
import os 
os.chdir('LTL/data/Prepare/Flair/')
from tqdm import tqdm ## tracks progress of loop ##
import  csv

'''UNCOMMENT OUT ONLY FOR NEW TAGGING
#Load the dataset
df = pd.read_csv('south_src.txt', sep="\n", header=None)
# df = pd.read_csv('north_src.txt', sep="\n", header=None)

# pd.options.display.max_colwidth = 150 #to view data in full
# text = "get to Blue Room , while avoiding The Ivy Room"

df = df.head(100)
#lowercase the source
#NOTE: remove after
#df[0] = df[0].str.lower()

print('NOTE DF HAS BEEN CUT FOR NOW')

#Covert the dataframe into a list of strings
text = df.to_string(index=False, header=False).split('\n')

#Test if worked: 
print('Dataframe:', len(df))
print('Text:', len(text))
# for i in range(10):
#     print(text[i])
#     print('-'*10)


# load the NER tagger
tagger = SequenceTagger.load('ner-ontonotes')
# tagger = SequenceTagger.load('ner-ontonotes')
print(tagger)


#ner is 4 class NER with higher accuracy and ner-ontonotes is a 18 class NER 
# run NER over sentence to identify entities
##method 2:
#f for storing NER tagged string
f_NER = []
span_NER = []
#run for every sentence 
# for i in tqdm(range(len(text))):
#     sentence = Sentence(text[i])
#     tagger.predict(sentence)
#     #append tagged sentence
#     f_NER.append(sentence.to_tagged_string())
# df1 = pd.read_csv('seq2tree/LTL/data/Prepare/Flair/NER_accuracy_gold.txt', sep="\n", header=None)
# df1[0] = df[0].str.rstrip(' .')
# text1 = df.head(100).to_string(index=False, header=False).splitlines()
lines = []
tags_pred = []

for i in tqdm(range(len(text))):
    sentence = Sentence(text[i])
    # print(sentence) #list of tokens 
    tagger.predict(sentence)
    # f_NER.append(sentence.to_tagged_string())
    # make list of predicted tags
    predicted_spans = sentence.get_spans('ner')
    sentence_pred = []
    for token in sentence:
        predicted_tag = 'O'

        # check if in predicted spans
        for span in predicted_spans:
            if token in span:
                predicted_tag = 'B-' + span.tag if token == span[0] else 'I-' + span.tag
        sentence_pred.append(predicted_tag)
        lines.append(f'{token.text} {predicted_tag}\n')
    lines.append('\n') #BIO
    tags_pred.append(sentence_pred) #all predictions
    # for entity in sentence.get_spans('ner'):
    #         span_NER.append(entity)
# print(tags_pred)
# print(lines)#file in bio format


# with open('tags_pred.txt', 'w') as filehandle:
#     for i in tags_pred:
#         filehandle.write(i)


with open("tags_pred.csv","w") as f:
    wr = csv.writer(f)
    wr.writerows(tags_pred)

'''
with open("tags_actual.csv","r") as f:
    tags_true = list(csv.reader(f))

with open("tags_pred.csv","r") as f:
    tags_pred = list(csv.reader(f))

#NOTE uncomment out if do not need 
loc = tags_pred
unlabeled = [[x.replace('PERSON','NE') for x in l] for l in loc]
unlabeled = [[x.replace('CARDINAL','NE') for x in l] for l in unlabeled]
unlabeled = [[x.replace('DATE','NE') for x in l] for l in unlabeled]
unlabeled = [[x.replace('FAC','NE') for x in l] for l in unlabeled]
unlabeled = [[x.replace('ORDINAL','NE') for x in l] for l in unlabeled]
unlabeled = [[x.replace('ORG','NE') for x in l] for l in unlabeled]
unlabeled = [[x.replace('EVENT','NE') for x in l] for l in unlabeled]
unlabeled = [[x.replace('NORP','NE') for x in l] for l in unlabeled]
unlabeled = [[x.replace('GPE','NE') for x in l] for l in unlabeled]
unlabeled = [[x.replace('LOC','NE') for x in l] for l in unlabeled]
unlabeled = [[x.replace('PRODUCT','NE') for x in l] for l in unlabeled]
unlabeled = [[x.replace('WORK_OF_ART','NE') for x in l] for l in unlabeled]
unlabeled = [[x.replace('LAW','NE') for x in l] for l in unlabeled]
unlabeled = [[x.replace('LANGUAGE','NE') for x in l] for l in unlabeled]
unlabeled = [[x.replace('TIME','NE') for x in l] for l in unlabeled]
unlabeled = [[x.replace('PERCENT','NE') for x in l] for l in unlabeled]
unlabeled = [[x.replace('MONEY','NE') for x in l] for l in unlabeled]
unlabeled = [[x.replace('QUANTITY','NE') for x in l] for l in unlabeled]

print(unlabeled)

with open("tags_unlabeled_pred.csv","w") as f:
    wr = csv.writer(f)
    wr.writerows(unlabeled)

with open("tags_unlabeled_actual.csv","r") as f:
    tags_true = list(csv.reader(f))

with open("tags_unlabeled_pred.csv","r") as f:
    tags_pred = list(csv.reader(f))


from seqeval.metrics import accuracy_score
from seqeval.metrics import classification_report
from seqeval.metrics import f1_score
print(tags_true)

print('F1-score:',f1_score(tags_true, tags_pred))
print('Accuracy:',accuracy_score(tags_true, tags_pred))
print(classification_report(tags_true, tags_pred))


# with open('listfile_flair_ner-ontonotes_new.txt', 'w') as filehandle:
#     for i in f_NER:
#         filehandle.write("%s\n" % i)


'''

# tagger.predict(sentence)

# sentence
# print('The following NER tags are found:')

# # iterate over entities and print
# for entity in sentence.get_spans('ner-ontonotes'):
#     print(entity)


# output = sentence.to_tagged_string()
# print(output)
# exit()
# with open('listfile_flair_ner-ontonotes.txt', 'w') as filehandle:
#     filehandle.writelines(output)
#     #for listitem in sentence.to_tagged_string():
#      #   filehandle.write('%s\n' % listitem)

# '''
# output = sentence.to_tagged_string()
# pickle.dump(output, open("flair_output.txt")'''