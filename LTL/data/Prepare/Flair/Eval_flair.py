
from flair.data import Sentence
from flair.models import SequenceTagger
import pandas as pd
import pickle
import os 
os.chdir('LTL/data/Prepare/Flair')
from tqdm import tqdm ## tracks progress of loop ##

# #Load the dataset input
src = pd.read_csv('south_src.txt', sep="\n", header=None)
src = src.head(100)

# ##load output
# tar = pd.read_csv('seq2tree/LTL/data/Prepare/Flair/listfile_flair_ner-ontonotes.txt', sep="\n", header=None)
tar_gold = pd.read_csv('NER_accuracy_gold.txt', sep="\n", header=None)


print(len(src))
print(len(tar_gold))
df3 = pd.merge(src, tar_gold, left_index=True, right_index=True)

# # define columns

from flair.data import Corpus
from flair.datasets import CSVClassificationCorpus

# # this is the folder in which train, test and dev files reside
data_folder = 'Data/'

# # column format indicating which columns hold the text and label(s)
column_name_map = {1: "text", 2: "ner"}
from flair.datasets import DataLoader, CONLL_03

# load the model you want to evaluate
tagger: SequenceTagger = SequenceTagger.load('ner-ontonotes')

# load the dataset

# load corpus containing training, test and dev data and if CSV has a header, you can skip it
corpus: Corpus = CSVClassificationCorpus(data_folder,
                                         column_name_map,
                                         skip_header=True,
                                         delimiter='\t',    # tab-separated files
)
# text = df.to_string(index=False, header=False).split('\n')

# from flair.datasets import DataLoader, CONLL_03

# load the dataset

# run evaluation procedure
import torch.utils.data as torch_d

result, score = tagger.evaluate(torch_d.DataLoader(df3, batch_size=1), out_path=f"predictions.txt")
print(result.log_line)