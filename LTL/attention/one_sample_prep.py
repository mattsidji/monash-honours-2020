from flair.data import Sentence
from flair.models import SequenceTagger
import pandas as pd
import pickle
import os 
from tqdm import tqdm ## tracks progress of loop ##

from util import entity_swap
import json



data_dir = 'LTL/data/unknown/'
# #Load the dataset input
def unknown_prep(data_dir):
    text = pd.read_csv(data_dir + 'test_one.txt', sep="\n", header=None).to_string(index=False, header=False).split('\n')
    # load the NER tagger
    #ner is 4 class NER with higher accuracy and ner-ontonotes is a 18 class NER 
    tagger = SequenceTagger.load('ner-ontonotes')
    # tagger = SequenceTagger.load('ner-ontonotes')
    # print(tagger)

    #f for storing NER tagged string
    f_NER = []
    # run for every sentence 
    for i in tqdm(range(len(text))):
        sentence = Sentence(text[i])
        tagger.predict(sentence)
        #append tagged sentence
        f_NER.append(sentence.to_tagged_string())
    text = f_NER
    #add \nto end of string 
    text = [item + " \n" for item in text]
    #remove spaces that are before the start of the string 
    text = list(map(str.lstrip, text))
    #use the entity swap function
    id_sent, key, key_label = entity_swap.__init__(text)

    #NOTE Lowercase everything
    test = [x.lower() for x in id_sent]
    key_low = []
    for i in range(len(key)):
            temp = key[i]
            temp = {k.lower(): v.lower() for k, v in temp.items()}
            key_low.append(temp)
    return test, key_low 

# test, key_low = unknown_prep(data_dir) 

# print(test)
# print(key_low)
# with open(data_dir + "test_one.txt", "w") as f:
#     f.writelines("%s" % items for items in test)
# with open(data_dir + 'test_one_dict.json', 'w') as fp:
#     json.dump(key_low, fp)