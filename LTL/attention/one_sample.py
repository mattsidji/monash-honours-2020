import argparse
import copy
from main import *
import torch
from tree import Tree
import json
from os import path, rename

from data_copy import serialize_data


def convert_to_string(idx_list, form_manager):
    w_list = []
    for i in range(len(idx_list)):
        w_list.append(form_manager.get_idx_symbol(int(idx_list[i])))
    return " ".join(w_list)


def do_generate(encoder, decoder, attention_decoder, enc_w_list, word_manager, form_manager, opt, using_gpu, checkpoint):
    # initialize the rnn state to all zeros
    prev_c  = torch.zeros((1, encoder.hidden_size), requires_grad=False)
    prev_h  = torch.zeros((1, encoder.hidden_size), requires_grad=False)
    if using_gpu:
        prev_c = prev_c.cuda()
        prev_h = prev_h.cuda()
    # reversed order
    enc_w_list.append(word_manager.get_symbol_idx('<S>'))
    enc_w_list.insert(0, word_manager.get_symbol_idx('<E>'))
    end = len(enc_w_list)
    enc_outputs = torch.zeros((1, end, encoder.hidden_size), requires_grad=False)
    if using_gpu:
        enc_outputs = enc_outputs.cuda()
    for i in range(end-1, -1, -1):
        cur_input = torch.tensor(np.array(enc_w_list[i]), dtype=torch.long)
        if using_gpu:
            cur_input = cur_input.cuda()
        prev_c, prev_h = encoder(cur_input, prev_c, prev_h)
        enc_outputs[:, i, :] = prev_h
    # decode
    queue_decode = []
    queue_decode.append({"s": (prev_c, prev_h), "parent":0, "child_index":1, "t": Tree()})
    head = 1
    while head <= len(queue_decode) and head <=100:
        s = queue_decode[head-1]["s"]
        parent_h = s[1]
        t = queue_decode[head-1]["t"]
        if head == 1:
            prev_word = torch.tensor([form_manager.get_symbol_idx('<S>')], dtype=torch.long)
        else:
            prev_word = torch.tensor([form_manager.get_symbol_idx('(')], dtype=torch.long)
        if using_gpu:
            prev_word = prev_word.cuda()
        i_child = 1
        while True:
            curr_c, curr_h = decoder(prev_word, s[0], s[1], parent_h)
            prediction, attention = attention_decoder(enc_outputs, curr_h)#add attention weight
            s = (curr_c, curr_h)
            _, _prev_word = prediction.max(1)
            # print (_prev_word)
            prev_word = _prev_word
            if int(prev_word[0]) == form_manager.get_symbol_idx('<E>') or t.num_children >= checkpoint["opt"].dec_seq_length:
                break
            elif int(prev_word[0]) == form_manager.get_symbol_idx('<N>'):
                #print("we predicted N");exit()
                queue_decode.append({"s": (s[0].clone(), s[1].clone()), "parent": head, "child_index":i_child, "t": Tree()})
                t.add_child(int(prev_word[0]))
            else:
                t.add_child(int(prev_word[0]))
            i_child = i_child + 1
        head = head + 1
    # refine the root tree (TODO, what is this doing?)
    for i in range(len(queue_decode)-1, 0, -1):
        cur = queue_decode[i]
        queue_decode[cur["parent"]-1]["t"].children[cur["child_index"]-1] = cur["t"]
    return queue_decode[0]["t"].to_list(form_manager), attention


if __name__ == "__main__":
    main_arg_parser = argparse.ArgumentParser(description="parser")
    main_arg_parser.add_argument('-gpuid', type=int, default=0, help='which gpu to use. -1 = use CPU')
    main_arg_parser.add_argument('-temperature', type=int, default=1, help='temperature of sampling')
    main_arg_parser.add_argument('-sample', type=int, default=0, help='0 to use max at each timestep (-beam_size=1), 1 to sample at each timestep, 2 to beam search')
    main_arg_parser.add_argument('-beam_size', type=int, default=20, help='beam size')
    main_arg_parser.add_argument('-display', type=int, default=1, help='whether display on console')
    main_arg_parser.add_argument('-data_dir', type=str, default='LTL/data/', help='data path')
    main_arg_parser.add_argument('-input', type=str, default='test.t7', help='input data filename')
    main_arg_parser.add_argument('-output', type=str, default='LTL/attention/output/seq2seq_output.txt', help='input data filename')
    main_arg_parser.add_argument('-model', type=str, default='LTL/attention/checkpoint_dir/model_seq2seq_noID', help='model checkpoint to use for sampling')
    main_arg_parser.add_argument('-seed',type=int,default=123,help='torch manual random number generator seed')
    #from data_copy
    main_arg_parser.add_argument("-train", type=str, default="train", help="train dir")
    main_arg_parser.add_argument("-test", type=str, default="test_one", help="test dir")
    main_arg_parser.add_argument("-min_freq", type=int, default=2, help="minimum word frequency")
    main_arg_parser.add_argument("-max_vocab_size", type=int, default=20000,help="max vocab size")
    
    #TODO:fix current bug with capitalisation
    main_arg_parser.add_argument('-unknown', type=int, default=0, help='whether to include unknown entities in framework')

    # parse input params
    args = main_arg_parser.parse_args()
    finished = False
    while not finished:
        print('waiting for instruction')
        #getting the utterance from mycroft 
        while not os.path.exists('mycroft-core/utterance.txt'):
            time.sleep(1)

        if os.path.isfile('mycroft-core/utterance.txt'):
            print('Processing utterance....')
            os.rename("mycroft-core/utterance.txt", "LTL/data/test_one.txt")
        

        if args.unknown > 0:
            from util import entity_swap
            from one_sample_prep import unknown_prep

            test, key_low = unknown_prep(args.data_dir)#take the input from test.txt in known file
            args.data_dir = args.data_dir + 'unknown/' 
            # print(test)
            # print(key_low)
            with open(args.data_dir + "test_one.txt", "w") as f:
                f.writelines("%s" % items for items in test)
            with open(args.data_dir + 'test_one_dict.json', 'w') as fp:
                json.dump(key_low, fp)


        #from data_copy
        # print('datadirectory: ', args.data_dir)
        random.seed(args.seed)
        np.random.seed(args.seed)
        torch.manual_seed(args.seed)
        serialize_data(args)


        # TODO, if the encoder was trained on a GPU do I need to call cuda
        using_gpu = False
        if args.gpuid > -1:
            using_gpu = True
        # load the model checkpoint
        if args.unknown > 0:
            checkpoint = torch.load('LTL/attention/checkpoint_dir/model_seq2seq_unknown')
        else:
            checkpoint = torch.load(args.model)
        encoder = checkpoint["encoder"]
        decoder = checkpoint["decoder"]
        attention_decoder = checkpoint["attention_decoder"]
        # put in eval mode for dropout
        encoder.eval()
        decoder.eval()
        attention_decoder.eval()
        # initialize the vocabulary manager to display text
        managers = pkl.load( open("{}/map.pkl".format(args.data_dir), "rb" ) )
        word_manager, form_manager = managers
        # load data
        data = pkl.load(open("{}/test.pkl".format(args.data_dir), "rb"))
        # print('this is data:', data)
        reference_list = []
        candidate_list = []
        candidate_str_list = []
        for i in range(len(data)):
            # print("example {}\n".format(i))
            x = data[i]
            # print('x is:', x)
            candidate, attention = do_generate(encoder, decoder, attention_decoder, x, word_manager, form_manager, args, using_gpu, checkpoint)
            candidate = [int(c) for c in candidate]

            num_left_paren = sum(1 for c in candidate if form_manager.idx2symbol[int(c)]== "(")
            num_right_paren = sum(1 for c in candidate if form_manager.idx2symbol[int(c)]== ")")
            diff = num_left_paren - num_right_paren
            #print(diff)
            if diff > 0:
                for i in range(diff):
                    candidate.append(form_manager.symbol2idx[")"])
            elif diff < 0:
                candidate = candidate[:diff]
            cand_str = convert_to_string(candidate, form_manager)
            candidate_list.append(candidate)
            candidate_str_list.append(cand_str)
            # print to console
            # if args.display > 0:
                # print(candidate_str_list)
                # # print("results: ")
                # print(ref_str)
                # # print(reference)
                # print(cand_str)
                # # print(candidate)
                # print(' ')
        
        if args.unknown > 0: 
            with open(args.data_dir + 'test_one_dict.json', 'r') as fp:
                dict_id = json.load(fp)
            # print(dict_id)
        
            # data = json.load(open('LTL/data/test_orderd_space_copy.json', 'r+'))
            # #print(pprint.pprint(data))
            # dict_id = entity_swap.extract_element(data, 'dict_id')
            #NOTE:Lowercase the dictionary 
            dict_id_lower = []
            for i in range(len(dict_id)):
                temp = dict_id[i]
                temp = {k.lower(): v.lower() for k, v in temp.items()}
                dict_id_lower.append(temp)
            
            #NOTE: Remove ID
            clean = entity_swap.__id_to_entity__(candidate_str_list, dict_id_lower)

            with open(args.output, "w") as f:
                for item in clean:
                    f.write("%s\n" % item)
        else: 
            with open(args.output, "w") as f:
                for item in candidate_str_list:
                    f.write("%s\n" % item)


        if args.display > 0:
            print('LTL after Semantic Parser: ', candidate_str_list)
            if args.unknown > 0:
                print('dictionary key: ', dict_id_lower)
                print('If used unkown LTL... final output: ', clean)