JAck makes changes

**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

## Installing GeneralTree 
    Wanting CONDA Download miniconda
    For installing Conda:
    Want to install miniconda because it’s lighter weight. Anaconda has a lot more stuff in it.
        https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html
    Confirm hash number
        https://docs.conda.io/projects/conda/en/latest/user-guide/install/download.html#hash-verification


    conda create --name <env> --file <this file>
    python LTL/attention/sample.py
    If needed: 
        pip install 
            numpy 
            torch
    
    #Issues with CUDA and torch.CDUA 
## Get CUDA 
$ sudo apt update
$ sudo apt install nvidia-cuda-toolkit

# Check CUDA VERSION
$ nvcc --version

## Drivers 
# Check if it is installed
ubuntu-drivers devices
   # if no driver 
    sudo apt install nvidia-driver-435 
# Update Existing Driver 
$ sudo ubuntu-drivers autoinstall 
--
reset computer 
--
Jay Broken


JAY RAN THIS TO MAKE THE CONDA ENV: conda create --name generalTree python=3.8 --file generalTree-env.txt 

 download CUDA 
    #updated driver (restart computer)
        Test1: nvidia-smi
        Test 2:
                python
                import torch    
                torch.cuda.is_available()

## Running General Tree
# Run pretrained model:
1. CD into LTL/attention
2. Write natural langauge input to data/test.txt
3. Go into conda environment
4. Run $ python data_copy.py
5. Run $ python one_sample.py
6. Desired output should be in output/seq2seq_output.txt

## Bring the MAVProxy startup script into your home directory so that it can be executed 
$ cp Jay/.mavinit.scr /home/user1/.mavinit.scr

$ cp Jay/ArduPilot/ArduCopter/worlds/my_ground_plane /.gazebo/models/my_ground_plane

## Within /Jay/ArduPilot/ArduCopeter in terminal run
gazebo --verbose worlds/iris_arducopter_map_brownuni.world 

## In a seperate terminal (in the same directory) run 
(sudo) ../Tools/autotest/sim_vehicle.py -f gazebo-iris -L BrownUniversity

## UPDATE! just run ./startup.sh to run both commands simulataneously 

## UPDATE for UPDATE, move .bashrc into home directory
from home directory 
. ~/.bashrc # to update the bashrc file
open_default_tabs # to automatically startup all tabs

## Get Mycroft working: 
# pull Mycroft version that works with code 
cd mycroft-core
git submodule init 
git submodule update

# set up mycroft (for more info https://mycroft-ai.gitbook.io/docs/using-mycroft-ai/get-mycroft)
bash dev_setup.sh
## Answer the setup questions Y,Y,N(Y if want local mimic),Y,Y

# Now that mycroft has been setup, change working directory to being the whole system 
cd .. 
# (into monash-honours-2020)
./mycroft-core/start-mycroft.sh debug


## To replace "hey Mycroft" with "hey drone"
Go to your user inside .Mycroft/mycroft.conf and replace with mycroft.conf provided in this repo

i.e. cp mycroft.conf /home/user1/.mycroft.conf

# if there is an mycroft update and you want to jump to the most up to date version (may cause conflicts)
git submodule update --remote 


## to add desktop tile
Go to open_tabs.desktop and edit the path to icon 
in file explorer GUI, copy and paste the open_tabs.desktop file onto the desktop

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).