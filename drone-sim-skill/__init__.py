from mycroft.skills.core import (MycroftSkill, intent_handler,
                                 intent_file_handler)
###intent_file_hander has depreciated and now we can use @intent_handler
#from mycroft.skills.core import MycroftSkill, intent_file_handler
from adapt.intent import IntentBuilder
#import re

class DroneSim(MycroftSkill):
    def __init__(self):
        MycroftSkill.__init__(self)

### Padatious intent handler WORKS ### 
    # @intent_handler('sim.drone.intent')
    # def handle_sim_drone_intent(self, message):
    #     self.speak_dialog("sim.drone")

### Apadt handler, triggered by keyword in DroneSimKeyword WORKS ###
    @intent_handler(IntentBuilder('').require('DroneSimKeyword'))
    def handle_sim_drone_intent(self, message):
        

    #the part from mycroft-speak that repeats the phrase
    # def speak_back(self, message):
        utterance = message.data.get('utterance')
        #repeat = re.sub('^.*?' + message.data['Speak'], '', utterance)
        self.speak("You said: ") 
        self.speak(utterance.strip())
        self.speak_dialog("sim.drone")
        
        with open("utterance.txt", "w") as save: 
            save.write("{}\n".format(utterance))

###tells the skill to turn off (its in all the other skills)
#def stop(self):
#        pass

def create_skill():
    return DroneSim()

### something that might be useful idk
    #def print_utterance(message):
    #print('Mycroft said "{}"'.format(message.data.get('utterance')))